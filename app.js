var os = require('os'),
    express = require('express'),
    app = express(),
    hostname = os.hostname();

app.use(function (req, res, next) {
    var csp = [
        "connect-src 'self'", // Fixes a rendering bug in iOS
        "script-src 'self'",
        "font-src https://fonts.gstatic.com"
    ].join('; ');

    res.set('Content-Security-Policy', csp);
    res.set('X-Content-Security-Policy', csp);
    res.set('X-Webkit-CSP', csp);

    res.set('X-Frame-Options', 'deny');
    res.set('X-XSS-Protection', '1; mode=block');
    res.set('X-Content-Type-Options', 'nosniff');
    res.set('Strict-Transport-Security', 'max-age=16070400; includeSubDomains');

    res.removeHeader('X-Powered-By');

    next();
});

app.use(express.static(__dirname + '/public'));

app.listen(process.env.PORT, function () {
    console.log('Listening for connections on :%s', process.env.PORT);
});
